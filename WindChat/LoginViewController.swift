//
//  ViewController.swift
//  WindChat
//
//  Created by d0t_iKs on 08/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    
    var user : User? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.hideKeyboardWhenTouchAround()
        self.EmailTextField.delegate = self
        self.PasswordTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Events
    
    func textFieldShouldReturn(_ textField : UITextField) -> Bool {
        textField.resignFirstResponder()
        switch textField {
        case PasswordTextField:
            PasswordTextField_Return()
            break
        default:
            break
        }
        return true;
    }
    
    @IBAction func EmailTextField_EditingDidEnd(_ sender: Any) {
        self.PasswordTextField.becomeFirstResponder()
    }
    
    
    @IBAction func SignInButton_PushUpInside(_ sender: Any) {
        performSegue(withIdentifier: "SignInSegue", sender: self)
    }
    
    @IBAction func LoginButton_TouchUpInside(_ sender: Any) {
        Login_Attempt(email: EmailTextField.text, password: PasswordTextField.text)
    }
    
    //Methods
    
    func PasswordTextField_Return(){
        Login_Attempt(email: EmailTextField.text, password: PasswordTextField.text)
    }
    
    
    func Login_Attempt(email : String?, password : String?){
        if (email! != "" && password != "")
        {
            user = User(email : EmailTextField.text, password : PasswordTextField.text)
            let gateway = NetworkGateway.sharedInstance
            gateway.Users?.UserLoginAsync(user: user!){
                result, error in
                if (error != nil || result?.token == nil){
                    self.ShowAlert(title: "Log In Attempt", message: "Log In Failed", button: "Dismiss")
                    return
                }
                self.user = result
                self.performSegue(withIdentifier: "LoginSegue", sender: self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "LoginSegue"){
            ((segue.destination) as? MainPageViewController)!.user = user
        }
        else if (segue.identifier == "SignInSegue"){
            ((segue.destination) as? SignInViewController)!.loginView = self
        }
    }
}

