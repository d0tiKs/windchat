//
//  SignInViewController.swift
//  WindChat
//
//  Created by d0t_iKs on 08/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class SignInViewController :  UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var UsernameTextField: UITextField!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var ConfirmTextField: UITextField!
    @IBOutlet weak var FirstNameTextField: UITextField!
    @IBOutlet weak var LastNameTextField: UITextField!
    @IBOutlet weak var BirthdayDatePicker: UIDatePicker!
    
    @IBOutlet weak var BottomConstraint: NSLayoutConstraint!
    
    
    var NCenter = NotificationCenter.default
    var activeField : UITextField!
    
    var loginView : LoginViewController?
    
    var user : User? = nil
    
    var kbSizeHeight : Float = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.hideKeyboardWhenTouchAround()
        // Do any additional setup after loading the view, typically from a nib.
        
        UsernameTextField.delegate = self
        FirstNameTextField.delegate = self
        LastNameTextField.delegate = self
        EmailTextField.delegate = self
        PasswordTextField.delegate = self
        ConfirmTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardWillChangeFrame), name: .UIKeyboardWillChangeFrame, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Events
    @IBAction func ConfirmButton_TouchUpInside(_ sender: Any) {
        confirmForm()
    }
    
    @IBAction func CancelButton_TouchUpInside(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //Methods
    
    func UsernameTextField_Return(){
        FirstNameTextField.becomeFirstResponder()
    }
    
    func FirstNameTextField_Return(){
        LastNameTextField.becomeFirstResponder()
    }
    
    func LastNameTextField_Return(){
        EmailTextField.becomeFirstResponder()
    }
    
    func PasswordTextField_Return(){
        ConfirmTextField.becomeFirstResponder()
    }
    
    func ConfirmTextField_Return(){
        confirmForm()
    }
    
    func textFieldShouldReturn(_ textField : UITextField) -> Bool {
        textField.resignFirstResponder()
        switch textField {
        case UsernameTextField :
            UsernameTextField_Return()
        case FirstNameTextField :
            FirstNameTextField_Return()
        case LastNameTextField :
            LastNameTextField_Return()
        case PasswordTextField :
            PasswordTextField_Return()
        case ConfirmTextField :
            ConfirmTextField_Return()
        default:
            break
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        activeField = nil
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func KeyboardWillShow(notification : NSNotification){
        AnimateTextFieldForKeyboard(notification: notification)
    }
    
    func KeyboardWillChangeFrame(notification : NSNotification){
        AnimateTextFieldForKeyboard(notification: notification)
    }
    
    func AnimateTextFieldForKeyboard(notification : NSNotification)
    {
        let userInfo = notification.userInfo! 
        let keyboardSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0 ,left: 0.0 , bottom: keyboardSize.height, right: 0.0)
        self.ScrollView.contentInset = contentInsets
        self.ScrollView.scrollIndicatorInsets = contentInsets
        
        if notification.name == NSNotification.Name.UIKeyboardWillShow
            || notification.name == NSNotification.Name.UIKeyboardWillChangeFrame {
            self.ScrollView.isScrollEnabled = true
            
            var aRect : CGRect = self.view.frame
            aRect.size.height -= keyboardSize.height
            if (self.activeField) != nil{
                if aRect.contains(activeField.frame.origin){
                    self.ScrollView.scrollRectToVisible(activeField.frame, animated: true)
                }
            }
            else{
                if (ScrollView.contentSize.height < ScrollView.frame.height){
                    ScrollView.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
                    ScrollView.isScrollEnabled = false
                }
                else{
                    ScrollView.isScrollEnabled = true
                }
            }
            // move up
        }
    }
    
    func checkForm() -> Bool {
        if (UsernameTextField.text!.isEmpty
            || FirstNameTextField.text!.isEmpty
            || LastNameTextField.text!.isEmpty
            || EmailTextField.text!.isEmpty
            || PasswordTextField.text!.isEmpty){
            
            ShowAlert(title: "Sign In Attempt", message: "Form Incomplete", button: "Dismiss")
            return false
        }
        
        if (PasswordTextField!.text != ConfirmTextField!.text){
            ShowAlert(title: "Sign In Attempt", message: "Password doesn't match", button: "Dismiss")
            return false
        }
        return true
    }
    
    func confirmForm(){
        if (checkForm()){
            user = User(username: UsernameTextField.text!, firstname: FirstNameTextField.text!, lastname: LastNameTextField.text!, birthday: BirthdayDatePicker.date, email: EmailTextField.text!, password: PasswordTextField.text!)
            let gateway = NetworkGateway.sharedInstance
            self.StartWaiting(title: "Sign In", message: "Please Wait")
            gateway.Users?.UserRegisterAsync(user: user!){
                result, error in
                
                if (error != nil){
                    self.ShowAlert(title: "Sign In Attempt", message: "Sign In Failed", button: "Dismiss")
                    self.StopWaiting()
                    return
                }
                self.user = result
                self.user?.password = self.PasswordTextField.text
                self.loginView?.user = self.user
                self.loginView?.PasswordTextField.text = self.PasswordTextField.text
                self.loginView?.EmailTextField.text = self.EmailTextField.text!
                self.StopWaiting()
                self.dismiss(animated: true, completion: nil)
            }
          
        }
    }
    
    func StartWaiting(title : String, message : String){
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.label.text = title;
        spinnerActivity.detailsLabel.text = message;
        spinnerActivity.isUserInteractionEnabled = false;
        
    }
    
    func StopWaiting(){
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
