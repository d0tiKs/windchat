//
//  Session.swift
//  WindChat
//
//  Created by d0t_iKs on 09/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation

class Session{
    var user : User
    var token : String?
    
    init(user : User){
        self.user = user
    }
}
