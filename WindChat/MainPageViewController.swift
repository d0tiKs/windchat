//
//  MainPageViewController.swift
//  WindChat
//
//  Created by d0t_iKs on 12/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import UIKit

class MainPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var identifiers: [String : UIViewController] = [:];
    var user : User?
    
    override func viewDidLoad() {
        
        identifiers["SettingsViewController"] = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController")
        identifiers["WindsViewController"] = self.storyboard?.instantiateViewController(withIdentifier: "WindsViewController")
        identifiers["WindViewerViewController"] = self.storyboard?.instantiateViewController(withIdentifier: "WindViewerViewController")
        
        (identifiers["WindsViewController"] as! WindsViewController).SetupGestures()
        (identifiers["WindsViewController"] as! WindsViewController).user = self.user
        (identifiers["WindViewerViewController"] as! WindViewerViewController).user = self.user
        (identifiers["SettingsViewController"] as! SettingsViewController).user = self.user
        (identifiers["SettingsViewController"] as! SettingsViewController).pageView = self
        
        self.dataSource = self
        self.delegate = self
        let viewControllers: NSArray = [identifiers["WindsViewController"]!]
        self.setViewControllers(viewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let identifier = viewController.restorationIdentifier

        if (identifier! == "SettingsViewController"){
            return identifiers["WindsViewController"]
        }
        else if (identifier! == "WindsViewController"){
            return identifiers["WindViewerViewController"]
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let identifier = viewController.restorationIdentifier
        
        if (identifier! == "WindsViewController"){
            return identifiers["SettingsViewController"]
        }
        else if (identifier! == "WindViewerViewController"){
            return identifiers["WindsViewController"]
        }
        
        return nil
    }
    
    
    private func presentationCountForPageViewController(pageViewController: UIPageViewController!) -> Int {
        return self.identifiers.count
    }
    
    private func presentationIndexForPageViewController(pageViewController: UIPageViewController!) -> Int {
        return 0
    }

    
}
