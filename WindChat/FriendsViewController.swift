//
//  FriendsViewController.swift
//  WindChat
//
//  Created by d0t_iKs on 12/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class FriendsViewController: UIViewController,
    UISearchBarDelegate, UIActionSheetDelegate,
    UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var SearchTableView: UITableView!
    @IBOutlet weak var DoneBarButton: UIButton!
    @IBOutlet weak var SearchBar: UISearchBar!
    
    var refreshControl: UIRefreshControl!
    
    let Sections = ["Search", "Pending", "Friends", "Blocked"]
    
    var user : User?
    
    var DataSource : [[User]] = [[],[],[],[]]
    
    var Friends : [User] {
        get {
            return DataSource[2]
        }
        set(value) {
            DataSource[2] = value
        }
    }
    
    var Pendings : [User] {
        get {
            return DataSource[1]
        }
        set(value) {
            DataSource[1] = value
        }
    }
    
    var Blocked : [User] {
        get {
            return DataSource[3]
        }
        set(value) {
            DataSource[3] = value
        }
    }
    
    var Users : [User] {
        get {
            return DataSource[0]
        }
        set(value) {
            DataSource[0] = value
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.hideKeyboardWhenTouchAround()
        
        SearchBar.delegate = self
        SearchTableView.delegate = self
        SearchTableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableCell")
        SearchTableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(PopulateDataSource), for: UIControlEvents.valueChanged)
        SearchTableView.addSubview(refreshControl)
        SearchTableView.tableFooterView = UIView()
        SearchTableView.separatorColor = UIColor.white
        
        PopulateDataSource()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
          return self.Sections[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.Sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.DataSource[section].count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)

        let user = self.DataSource[indexPath.section][indexPath.row]
        cell.textLabel?.text = user.username
        
        user.SmallPictureGetAsync(){
            data in
            if (data != nil){
                cell.imageView?.image = UIImage(data: data as! Data)
                cell.imageView?.layer.cornerRadius = 25
                cell.imageView?.layer.masksToBounds = true
                 self.SearchTableView.performSelector(onMainThread: #selector( self.SearchTableView.reloadData), with:nil, waitUntilDone:true)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) ->[UITableViewRowAction]? {
        
        if indexPath.section == 0{
            let add = UITableViewRowAction(style: .normal, title: "Add") {
                action, index in
                self.addFriend(user: self.DataSource[indexPath.section][indexPath.row])
            }
            add.backgroundColor = UIColor(red:71/255.0, green:231/255.0, blue:118/255.0, alpha: 1)
            
            return [add]
        }
        else if indexPath.section == 1{
            let accept = UITableViewRowAction(style: .normal, title: "Accept") {
                action, index in
                self.responseFriend(user: self.DataSource[indexPath.section][indexPath.row], accepted: true)
            }
            accept.backgroundColor = UIColor(red:71/255.0, green:231/255.0, blue:118/255.0, alpha: 1)
            
            let decline = UITableViewRowAction(style: .normal, title: "Decline") {
                action, index in
                self.responseFriend(user: self.DataSource[indexPath.section][indexPath.row], accepted: false)
            }
            decline.backgroundColor = UIColor(red:237/255.0, green:57/255.0, blue:54/255.0, alpha: 1)
            
            return [accept, decline]
        }
        else if indexPath.section == 2{
            let block = UITableViewRowAction(style: .normal, title: "Block") {
                action, index in
                self.blockFriend(user: self.DataSource[indexPath.section][indexPath.row], blocked: true)
            }
            block.backgroundColor = UIColor.lightGray
            
            let remove = UITableViewRowAction(style: .normal, title: "Remove") {
                action, index in
                self.removeFriend(user: self.DataSource[indexPath.section][indexPath.row])
            }
            remove.backgroundColor = UIColor(red:237/255.0, green:57/255.0, blue:54/255.0, alpha: 1)
            return [remove, block]
        }
        else{
            let remove = UITableViewRowAction(style: .normal, title: "remove") {
                action, index in
                self.blockFriend(user: self.DataSource[indexPath.section][indexPath.row], blocked: false)
            }
            remove.backgroundColor = UIColor(red:237/255.0, green:57/255.0, blue:54/255.0, alpha: 1)
            return [remove]
        }
    }
    
    func StartWaiting(title : String, message : String){
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.label.text = title;
        spinnerActivity.detailsLabel.text = message;
        spinnerActivity.isUserInteractionEnabled = false;
        
        SearchBar.isUserInteractionEnabled = false
    }
    
    func StopWaiting(){
        MBProgressHUD.hide(for: self.view, animated: true)
        SearchBar.isUserInteractionEnabled = true
    }
    
    func PopulateDataSource(){
        let gateway = NetworkGateway.sharedInstance
        gateway.Users?.FriendsListAsync(){
            friends, error in
            self.StartWaiting(title: "Loading Friends", message : "Please Wait")
            if (error != nil){
                print(error as Any)
                self.ShowAlert(title: "Retrieve Friends", message: "Failed", button: "Dismiss")
                self.refreshControl.endRefreshing()
                self.StopWaiting()
                return
            }
            if (friends != nil){
                self.Friends.removeAll()
                for user in friends! {
                    if(!user.isEmpty()){
                        self.Friends.append(user)
                    }
                }
                self.SearchTableView.reloadData()
            }
            self.StopWaiting()
        }
        
        gateway.Users?.PendingListAsync(){
            pending, error in
            self.StartWaiting(title: "Loading Pendings", message : "Please Wait")
            if (error != nil){
                print(error as Any)
                self.ShowAlert(title: "Retrieve Pendings", message: "Failed", button: "Dismiss")
                self.refreshControl.endRefreshing()
                self.StopWaiting()
                return
            }
            if (pending != nil){
                self.Pendings.removeAll()
                for user in pending! {
                    if(!user.isEmpty()){
                        self.Pendings.append(user)
                    }
                }
                self.SearchTableView.reloadData()
            }
            self.StopWaiting()
        }
        
        gateway.Users?.BlockedListAsync(){
            blocked, error in
            self.StartWaiting(title: "Loading Friends", message : "Please Wait")
            if (error != nil){
                print(error as Any)
                self.ShowAlert(title: "Retrieve Blocked", message: "Failed", button: "Dismiss")
                self.refreshControl.endRefreshing()
                self.StopWaiting()
                return
            }
            else if (blocked != nil){
                self.Blocked.removeAll()
                for user in blocked! {
                    if(!user.isEmpty()){
                        self.Blocked.append(user)
                    }
                }
                self.SearchTableView.reloadData()
            }
            self.StopWaiting()
            self.refreshControl.endRefreshing()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if (searchBar.text == ""){
            return
        }
        let gateway = NetworkGateway.sharedInstance
        gateway.Users?.UserSearchAsync(username: SearchBar.text!) {
            users, error in
            
            self.StartWaiting(title: "Users", message: "Loading Users")
            
            if (error != nil){
                self.ShowAlert(title: "Search for friend", message: "Search Failed", button: "Dismiss")
                self.StopWaiting()
                return
            }
            self.Users.removeAll()
            for user in users! {
                self.Users.append(user)
            }
            self.SearchTableView.reloadData()
            self.StopWaiting()
        }
        dismissKeyboard()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        dismissKeyboard()
    }
    
    func responseFriend(user : User, accepted : Bool){
        let gateway = NetworkGateway.sharedInstance
        gateway.Users?.FriendResponseAsync(id: user.id!, accepted: accepted){
            result, error in
            self.StartWaiting(title: "Sending Answer", message : "Please Wait")
            if (error != nil || result == nil){
                self.ShowAlert(title: "Friendship Answer", message: "Failed", button: "Dismiss")
                self.StopWaiting()
                return
            }
            self.PopulateDataSource()
            self.SearchTableView.reloadData()
            self.StopWaiting()
        }
    }
    
    func blockFriend(user : User, blocked : Bool){
        let gateway = NetworkGateway.sharedInstance
        gateway.Users?.FriendBlockAsync(id: user.id!, blocked: blocked){
            result, error in
            self.StartWaiting(title: "Blocking User", message : "Please Wait")
            if (error != nil || result == nil){
                self.ShowAlert(title: "Blocking User", message: "Failed", button: "Dismiss")
                self.StopWaiting()
                return
            }
            self.PopulateDataSource()
            self.SearchTableView.reloadData()
            self.StopWaiting()
        }
    }

    
    func removeFriend(user : User){
        let gateway = NetworkGateway.sharedInstance
        gateway.Users?.FriendDeleteAsync(id: user.id!){
            result, error in
            self.StartWaiting(title: "Removing Friend", message : "Please Wait")
            if (error != nil || result == nil){
                self.ShowAlert(title: "Removing Friend", message: "Failed", button: "Dismiss")
                            self.StopWaiting()
                return
            }
            self.PopulateDataSource()
            self.SearchTableView.reloadData()
            self.StopWaiting()
        }
    }

    
    func addFriend(user : User){
        let gateway = NetworkGateway.sharedInstance
        gateway.Users?.FriendAddAsync(username: user.username!){
            result, error in
            self.StartWaiting(title: "Loading Friends", message : "Please Wait")
            if (error != nil || result == nil){
                self.ShowAlert(title: "Add Friend", message: "Failed", button: "Dismiss")
                self.StopWaiting()
                return
            }
            self.PopulateDataSource()
            self.SearchTableView.reloadData()
            self.StopWaiting()
        }
    }
    
    
    
    @IBAction func DoneButon_TouchUpInside(_ sender: Any) {
        DismissView()
    }
    
    func DismissView(){
        self.dismiss(animated: true, completion: nil)
    }
}
