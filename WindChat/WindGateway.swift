//
//  WindGateway.swift
//  WindChat
//
//  Created by d0t_iKs on 17/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import NetworkExtension
import CFNetwork
import ObjectMapper
import Alamofire

enum WindError : Error {
    case WIND_OPEN_ERROR(String?)
    case WIND_SEND_ERROR(String?)
    case WIND_LIST_ERROR(String?)
}

class WindGateway {
    
    var user_ : User?
    {
        get{
            return gateway_.Users?.user_
        }
    }
    
    let gateway_ : NetworkGateway
    
    init (gateway : NetworkGateway){
        self.gateway_ = gateway
    }
    
    func WindsListAsync(user : User,
                        completionHandler:@escaping(_ users : [User]?, _ error : Any?) -> Void){
        gateway_.SendHttpMethodArrayAsync(object: nil as User?, auth: user_?.token, httpMethod: .get, requestPath: "wind/list"){
           data, error in
            if (error != nil){
                completionHandler([], WindError.WIND_LIST_ERROR)
                return
            }
            completionHandler(Mapper<User>().mapArray(JSONArray: data!), nil)
        }
    }
    
    func WindOpenAsync(user : User, wind : Wind,
                       completionHandler:@escaping(_ result : Bool?, _ error : Any?) -> Void){
        gateway_.SendHttpMethodAsync(object: nil as Wind?, auth: user_?.token, httpMethod: .put, requestPath: "wind/\(wind.id!)/open"){
            result, error in
            if (error != nil){
                completionHandler(nil, WindError.WIND_OPEN_ERROR)
                return
            }
            completionHandler((result as! [String : Bool])["value"]!, nil)
        }
    }
    
    func WindSendAsync(user : User, wind : Wind, recipitens : [Int],
                        completionHandler:@escaping(_ result : Bool?, _ error : Any?) -> Void){
        gateway_.SendHttpMethodAsync(object: wind, auth: user_?.token, httpMethod: .post, requestPath: "wind"){
            result, error in
            if (error != nil){
                completionHandler(nil, WindError.WIND_SEND_ERROR)
                return
            }
              completionHandler((result as! [String : Bool])["value"]!, nil)
        }
    }
}
