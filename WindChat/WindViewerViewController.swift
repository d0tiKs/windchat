//
//  ChatViewController.swift
//  WindChat
//
//  Created by d0t_iKs on 12/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class WindViewerViewController: UIViewController, UIActionSheetDelegate,
UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var WindTableView: UITableView!
    
    var Sections : [String] = []
    
    var Users :  [User] = []
    var DataSource : [[Wind]] = []
    
    var user : User?
    
    var refreshControl: UIRefreshControl!
    
    var selectedWind : Wind?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        WindTableView.delegate = self
        WindTableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableCell")
        WindTableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(PopulateDataSource), for: UIControlEvents.valueChanged)
        
        WindTableView.addSubview(refreshControl)
        WindTableView.tableFooterView = UIView()
        WindTableView.separatorColor = UIColor.white
        
        PopulateDataSource()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.Sections[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.Sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.DataSource[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
        let wind = DataSource[indexPath.section][indexPath.row]
        cell.textLabel?.text = "Duration : \(wind.duration!)"
        
        if (wind.isOpened)!{
            cell.imageView?.image = #imageLiteral(resourceName: "Wind Seen")
        }
        else{
            cell.imageView?.image = #imageLiteral(resourceName: "Wind New")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myCustomView = UIImageView(frame: CGRect(x: 5, y: 8, width: 45, height: 40))
        let user = self.Users[section]
        let header = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! CustomHeaderCell
        
        user.SmallPictureGetAsync(){
            data in
            if (data != nil){
                myCustomView.image = UIImage(data: data as! Data)
                myCustomView.layer.cornerRadius = 25
                myCustomView.layer.masksToBounds = true
                self.WindTableView.layoutSubviews()
            }
        }
        
        header.contentView.addSubview(myCustomView)
        header.headerLabel?.text = user.username!
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedWind = DataSource[indexPath.section][indexPath.row]
        if (!(selectedWind?.isOpened)!){
            performSegue(withIdentifier: "OpenWindSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OpenWindSegue" {
            let controller = segue.destination as! WindOpenerViewController
            controller.wind = selectedWind
            controller.user = user
            controller.main = self
        }
    }
    
    func StartWaiting(title : String, message : String){
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.label.text = title;
        spinnerActivity.detailsLabel.text = message;
        spinnerActivity.isUserInteractionEnabled = false;
    }
    
    func StopWaiting(){
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func PopulateDataSource(){
        let gateway = NetworkGateway.sharedInstance
        self.StartWaiting(title: "Loading Winds", message : "Please Wait")
        gateway.Winds?.WindsListAsync(user: user!){
            users, error in
            if (error != nil){
                print(error as Any)
                self.ShowAlert(title: "Retrieve Winds", message: "Failed", button: "Dismiss")
                self.refreshControl.endRefreshing()
                self.StopWaiting()
                return
            }
            
            self.Sections.removeAll()
            self.DataSource.removeAll()
            self.Users.removeAll()
            var count : Int = 0
            for user in users! {
                self.Sections.append(user.username!)
                self.DataSource.append([])
                self.Users.append(user)
                for wind in user.winds!{
                    self.DataSource[count].append(wind)
                }
                self.DataSource[count].sort() {$0.sendDate! < $1.sendDate!}
                count += 1
            }
            
            self.WindTableView.reloadData()
            self.refreshControl.endRefreshing()
            self.StopWaiting()
        }
    }
}
