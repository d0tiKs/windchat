//
//  Extensions.swift
//  WindChat
//
//  Created by d0t_iKs on 08/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import UIKit

extension UIViewController{
    func hideKeyboardWhenTouchAround(){
        let tap_around: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap_around)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func ShowAlert(title : String, message : String, button: String)
    {
        let alert = UIAlertController(title : title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
