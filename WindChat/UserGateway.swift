//
//  LoginManager.swift
//  WindChat
//
//  Created by d0t_iKs on 08/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import NetworkExtension
import CFNetwork
import ObjectMapper
import Alamofire

enum UserError : Error {
    case REGISTER_ERROR(String?)
    case LOGIN_ERROR(String?)
    case USER_LIST_ERROR(String?)
    case USER_SEARCH_ERROR(String?)
    case FRIEND_RESPONSE_ERROR(String?)
    case FRIEND_ADD_ERROR(String?)
    case FRIEND_DELETE_ERROR(String?)
    case FRIEND_BLOCK_ERROR(String?)
}

class UserGateway {
    
    var user_ : User? = nil
    let gateway_ : NetworkGateway
    
    init (gateway : NetworkGateway){
        self.gateway_ = gateway
    }
    
    func UserRegisterAsync(user : User,
                           completionHandler:@escaping (_ user: User?, _ error : Any?) -> Void){
        gateway_.SendHttpMethodAsync(object: user, httpMethod: HTTPMethod.post, requestPath: "user/register") { user, error in
            if (error != nil || user == nil){
                completionHandler(nil, UserError.REGISTER_ERROR)
                return
            }
             completionHandler(Mapper<User>().map(JSON: user!), nil)
        }
    }
    
    func UserLoginAsync(user : User,
                        completionHandler:@escaping(_ user : User?, _ error : Any?) -> Void){
        gateway_.SendHttpMethodAsync(object: user, httpMethod: HTTPMethod.post, requestPath: "user/login")
        { user, error in
            if (error != nil || user == nil){
                completionHandler(nil, UserError.LOGIN_ERROR)
                return
            }
            self.user_ = Mapper<User>().map(JSON: user!)
            completionHandler(self.user_, nil)
        }
    }
    
    func FriendsListAsync(completionHandler:@escaping(_ bloqued : [User]?, _ error : Any?) -> Void){
        gateway_.SendHttpMethodArrayAsync(object: nil as User?, auth: user_?.token, httpMethod: HTTPMethod.get, requestPath: "friend/list"){
            friends, error in
            if (error != nil){
                completionHandler([], UserError.USER_LIST_ERROR)
                return
            }
            completionHandler(Mapper<User>().mapArray(JSONArray: friends!), nil)
        }
    }
    
    func BlockedListAsync(completionHandler:@escaping(_ bloqued : [User]?, _ error : Any?) -> Void){
        gateway_.SendHttpMethodArrayAsync(object: nil as User?, auth: user_?.token, httpMethod: HTTPMethod.get, requestPath: "friend/blockedList"){
            blocked, error in
            if (error != nil){
                completionHandler([], UserError.USER_LIST_ERROR)
                return
            }
            completionHandler(Mapper<User>().mapArray(JSONArray: blocked!), error)
        }
    }
    
    func PendingListAsync(completionHandler:@escaping(_ bloqued : [User]?, _ error : Any?) -> Void){
        gateway_.SendHttpMethodArrayAsync(object: nil as User?, auth: user_?.token, httpMethod: HTTPMethod.get, requestPath: "friend/pendingList"){
            pending, error in
            if (error != nil){
                completionHandler([], UserError.USER_LIST_ERROR)
                return
            }
            completionHandler(Mapper<User>().mapArray(JSONArray: pending!), error)
        }
    }
    
    func UserSearchAsync(username : String,
                         completionHandler:@escaping(_ friends : [User]?, _ error : Any?) -> Void){
        gateway_.SendHttpMethodArrayAsync(object: nil as User?, auth: user_?.token,
                                          httpMethod: HTTPMethod.get, requestPath: "user/search/" + username){
            users, error in
            if (error != nil){
                completionHandler([], UserError.USER_SEARCH_ERROR)
                return
            }
            completionHandler(Mapper<User>().mapArray(JSONArray: users!), error)
        }
    }
    
    func FriendAddAsync(username : String,
                         completionHandler:@escaping(_ result  :  Bool?,  _ error : Any?) -> Void){
        
        let body = ["userName" : username]
        
        gateway_.SendHttpMethodAsync(body : body ,auth: user_?.token, httpMethod: HTTPMethod.post, requestPath: "friend/"){
            result, error in
            if(error != nil || result == nil){
                completionHandler(nil, UserError.FRIEND_ADD_ERROR)
                return
            }
            completionHandler((result as! [String : Bool])["value"]!, nil)
        }
    }
    
    func FriendDeleteAsync(id : Int,
                        completionHandler:@escaping(_ result  :  Bool?,  _ error : Any?) -> Void){
        
        gateway_.SendHttpMethodAsync(body : nil, auth: user_?.token, httpMethod: HTTPMethod.delete, requestPath: "friend/\(id)"){
            result, error in
            if(error != nil || result == nil){
                completionHandler(nil, UserError.FRIEND_DELETE_ERROR)
                return
            }
            completionHandler((result as! [String : Bool])["value"]!, nil)
        }
    }
    
    func FriendBlockAsync(id : Int, blocked : Bool,
                             completionHandler:@escaping(_ result : Bool?, _ error : Any?) -> Void){
        
        let body = ["isBlocked": blocked]
        
        gateway_.SendHttpMethodAsync(body : body ,auth: user_?.token, httpMethod: HTTPMethod.put, requestPath: "friend/\(id)/block"){
            result, error in
            if(error != nil || result == nil){
                completionHandler(nil, UserError.FRIEND_BLOCK_ERROR)
                return
            }
            var res = result as! [String : Bool]
            completionHandler(res["value"], nil)
        }
    }
    
    func FriendResponseAsync(id : Int, accepted : Bool,
                           completionHandler:@escaping(_ result : Bool?, _ error : Any?) -> Void){
        
        let body = ["isAccepted": accepted]
        
        gateway_.SendHttpMethodAsync(body : body ,auth: user_?.token, httpMethod: HTTPMethod.put, requestPath: "friend/\(id)/accept"){
            result, error in
            if(error != nil || result == nil){
                completionHandler(nil, UserError.FRIEND_RESPONSE_ERROR)
                return
            }
            var res = result as! [String : Bool]
            completionHandler(res["value"], nil)
        }
    }
}
