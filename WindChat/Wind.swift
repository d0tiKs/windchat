//
//  Wind.swift
//  WindChat
//
//  Created by d0t_iKs on 17/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation
import ObjectMapper

class Wind : Mappable{
    
    var id : Int?
    var duration : Int?
    var longitude : Float?
    var latitude : Float?
    var sendDate : Date?
    var isOpened : Bool?
    
    var recipients : [Int]?
    var user : User?
    
    private var image : NSData?
    var imageStr64 : String?
    var imageUrl : String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    init(user : User, recipients: [Int], duration : Int, image : NSData){
        self.user = user
        self.recipients = recipients
        self.duration = duration
        self.imageStr64 = image.base64EncodedString()
        self.image = image
    }
    
    init(user: User, duration : Int, imageUrl : String){
        self.user = user
        self.duration = duration
        self.imageUrl = imageUrl
    }
    
    func ImageGetAsync(completionHandler:@escaping(NSData?) -> Void){
        if (image != nil){
            completionHandler(image)
        }
        else if(imageUrl != nil){
            let gateway = NetworkGateway.sharedInstance
            gateway.LoadImageAsync(url: imageUrl!){
                data in
                self.image = data
                completionHandler(self.image)
            }
        }
        else{
           completionHandler(nil)
        }
    }
    
    func mapping(map: Map) {

        id           <-  map["id"]
        duration     <-  map["duration"]
        longitude    <-  map["longitude"]
        latitude     <-  map["latitude"]
        recipients   <-  map["recipients"]
        imageUrl     <-  map["imageUrl"]
        sendDate     <-  (map["sendDate"], DateTransform())
        isOpened     <-  map["isOpened"]
        imageStr64   <-  map["image"]
    }

}
