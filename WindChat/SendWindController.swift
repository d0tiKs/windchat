//
//  SendWindController.swift
//  WindChat
//
//  Created by d0t_iKs on 18/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class SendWindController: UIViewController,
    UISearchBarDelegate, UIActionSheetDelegate,
UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var ProgressBar: UISlider!
    @IBOutlet weak var SearchTableView: UITableView!
    @IBOutlet weak var DoneBarButton: UIButton!
    @IBOutlet weak var PreviewImageView: UIImageView!
    @IBOutlet weak var SendButton: UIButton!
    
    var image : UIImage?

    
    var refreshControl: UIRefreshControl!
    
    let Sections = ["Friends", "Queue"]
    
    var user : User?

    var DataSource : [[User]] = [[],[]]
    
    var Friends : [User] {
        get {
            return DataSource[0]
        }
        set(value) {
            DataSource[0] = value
        }
    }
    
    var Queue : [User] {
        get {
            return DataSource[1]
        }
        set(value) {
            DataSource[1] = value
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        PreviewImageView.image = image!
        
        SearchTableView.delegate = self
        SearchTableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableCell")
        SearchTableView.dataSource = self

        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(PopulateDataSource), for: UIControlEvents.valueChanged)
        SearchTableView.addSubview(refreshControl)
        SearchTableView.tableFooterView = UIView()
        SearchTableView.separatorColor = UIColor.white
        
        PopulateDataSource()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.Sections[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.Sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.DataSource[section].count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
        
        let user = self.DataSource[indexPath.section][indexPath.row]
        cell.textLabel?.text = user.username
        
        user.SmallPictureGetAsync(){
            data in
            if (data != nil){
                cell.imageView?.image = UIImage(data: data as! Data)
                self.SearchTableView.performSelector(onMainThread: #selector( self.SearchTableView.reloadData), with:nil, waitUntilDone:true)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) ->[UITableViewRowAction]? {
        
        if indexPath.section == 0{
            let add = UITableViewRowAction(style: .normal, title: "Add") {
                action, index in
                self.Queue.append(self.DataSource[indexPath.section][indexPath.row])
                self.Friends.remove(at: indexPath.row)
                self.SearchTableView.reloadData()
            }
            add.backgroundColor = UIColor(red:65/255.0, green:155/255.0, blue:249/255.0, alpha: 1)
            return [add]
        }
        else if indexPath.section == 1{
            let remove = UITableViewRowAction(style: .normal, title: "Remove") {
                action, index in
                self.Friends.append(self.DataSource[indexPath.section][indexPath.row])
                self.Queue.remove(at: indexPath.row)
                self.SearchTableView.reloadData()
             }
            remove.backgroundColor = UIColor(red:223/255.0, green:26/255.0, blue:33/255.0, alpha: 1)
            return [remove]
        }
        return nil
    }
    
    @IBAction func SendButton_TouchUpInside(_ sender: Any) {
        SendWindAsync()
    }
    
    func SendWindAsync() {
        StartWaiting(title: "Sending Wind", message: "Please Wait")
        var recipients: [Int] = []
        for user in Queue {
            recipients.append(user.id!)
        }
        let wind = Wind(user: user!, recipients: recipients, duration: Int(ProgressBar.value),
                        image: (UIImageJPEGRepresentation(image!, 50) as NSData?)!)
        
        let gateway = NetworkGateway.sharedInstance
        gateway.Winds?.WindSendAsync(user: user!, wind: wind, recipitens: recipients){
            result, error in
            if (result == false || result == nil || error != nil){
                print(error as Any)
                self.ShowAlert(title: "Send Wind", message: "Fail to contact server", button: "Dismiss")
            }
            self.StopWaiting()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func StartWaiting(title : String, message : String){
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.label.text = title;
        spinnerActivity.detailsLabel.text = message;
        spinnerActivity.isUserInteractionEnabled = false;
    }
    
    func StopWaiting(){
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func PopulateDataSource(){
        let gateway = NetworkGateway.sharedInstance
        gateway.Users?.FriendsListAsync(){
            friends, error in
            self.StartWaiting(title: "Loading Friends", message : "Please Wait")
            if (error != nil){
                print(error as Any)
                self.ShowAlert(title: "Retrieve Friends", message: "Failed", button: "Dismiss")
                self.refreshControl.endRefreshing()
                self.StopWaiting()
                return
            }
            self.Friends.removeAll()
            self.Queue.removeAll()
            if (friends != nil){
                for user in friends! {
                    if(!user.isEmpty()){
                        self.Friends.append(user)
                    }
                }
                self.SearchTableView.reloadData()
            }
            self.StopWaiting()
        }
    }

    
    @IBAction func DoneButon_TouchUpInside(_ sender: Any) {
        DismissView()
    }
    
    func DismissView(){
        self.dismiss(animated: true, completion: nil)
    }
}
