//
//  SettingsViewController.swift
//  WindChat
//
//  Created by d0t_iKs on 12/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation
import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var UsernameLabel: UILabel!
    @IBOutlet weak var PictureImageView: UIImageView!
    
    var user : User?
    var pageView : MainPageViewController?
    
    override func viewDidLoad() {
        PictureImageView.layer.cornerRadius = 25
        PictureImageView.layer.masksToBounds = true
        
        user?.PictureGetAsync(){
            data in
            if (data != nil){
                self.PictureImageView.image = UIImage(data: data as! Data)
            }
        }
        UsernameLabel.text = user?.username
    }
    
    @IBAction func Disconnet_TouchUpInside(_ sender: Any) {
        parent?.dismiss(animated: true, completion: nil)
    }
}
