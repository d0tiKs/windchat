//
//  NetworkManager.swift
//  WindChat
//
//  Created by d0t_iKs on 08/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import NetworkExtension
import CFNetwork

import Alamofire
import ObjectMapper

class NetworkGateway : NSObject, NSURLConnectionDelegate{
    
    let apiUrl_ = "http://windchatapi.3ie.fr/api/"
    let apiKey_ = "{ZBVI:PN74F-RFUVS;PR:Z-H:@CQTGNV:}"
    
    static let sharedInstance = NetworkGateway()
    
    static var users_ : UserGateway?
    var Users : UserGateway? {
        get {
            return NetworkGateway.users_
        }
    }
    
    static var winds_ : WindGateway?
    var Winds : WindGateway? {
        get {
            return NetworkGateway.winds_
        }
    }
    
    override init()
    {
        super.init()
        NetworkGateway.users_ = UserGateway(gateway: self)
        NetworkGateway.winds_ = WindGateway(gateway: self)
    }
    
    func LoadImageAsync(url : String, completionHander:@escaping (_ data : NSData?) -> Void){
        Alamofire.request(url, method: .get).responseData {
            response in
            if (response.result.error == nil){
                completionHander(response.data as NSData?)
            }
        }
    }
    
    func SendHttpMethodAsync<T : Mappable>(object : T? = nil, auth : String? = nil, httpMethod : HTTPMethod, requestPath : String, completionHandler:@escaping (_ object: [String: Any]?, _ error : Any? ) -> Void) {
        
        let url : String = apiUrl_ + requestPath
        
        var headers : [String : String] =  ["Content-Type" : "application/json", "X-Api-Key" : apiKey_ ]
        
        if (auth != nil){
            headers["Authorization"] = "Bearer " + auth!
        }
        
        let parameters : [String : Any]? = object?.toJSON()
        
        Alamofire.request(url, method: httpMethod, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{
            response in
            if response.result.isSuccess{
                let data = response.result.value as? [String: Any]
               if (data != nil && !(data?.isEmpty)! && (data?.keys.contains("message"))!){
                    print(data?["message"] as Any)
                    completionHandler(nil, data?["message"])
                }
                else {
                    completionHandler(data, nil)
                }
            }
            else{
                print(response.result.error as Any)
                completionHandler(nil, response.result.error)
            }
        }
    }
    
    func SendHttpMethodAsync(body : [String : Any]?, auth : String? = nil, httpMethod : HTTPMethod, requestPath : String, completionHandler:@escaping (_ object: Any?, _ error : Any? ) -> Void) {
        
        let url : String = apiUrl_ + requestPath
        
        var headers : [String : String] =  ["Content-Type" : "application/json", "X-Api-Key" : apiKey_ ]
        
        if (auth != nil){
            headers["Authorization"] = "Bearer " + auth!
        }
        
        Alamofire.request(url, method: httpMethod, parameters: body, encoding: JSONEncoding.default, headers: headers).responseJSON{
            response in
            if response.result.isSuccess{
                let data = response.result.value as? [String: Any]
                if (data != nil && !(data?.isEmpty)! && (data?.keys.contains("message"))!){
                    print(data?["message"] as Any)
                    completionHandler(nil, data?["message"])
                }
                else {
                    completionHandler(data, nil)
                }
            }
            else{
                print(response.result.error as Any)
                completionHandler(nil, response.result.error)
            }
        }
    }
    
    func SendHttpMethodArrayAsync<T : Mappable>(object : T? = nil, auth : String? = nil, httpMethod : HTTPMethod, requestPath : String, completionHandler:@escaping (_ object: [[String: Any]]?, _ error : Any?) -> Void) {

        let url : String = apiUrl_ + requestPath
        
        var headers : [String : String] =  ["Content-Type" : "application/json", "X-Api-Key" : apiKey_ ]
        
        if (auth != nil){
            headers["Authorization"] = "Bearer " + auth!
        }
        
        let parameters : [String : Any]? = object?.toJSON()
        
        Alamofire.request(url, method: httpMethod, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{
            response in
            if response.result.isSuccess{
                
                let message = response.result.value as? [String: Any]
                let data = response.result.value as? [[String: Any]]
                if ((data != nil && !(data?.isEmpty)! && (data?[0].keys.contains("message"))!)){

                    print(data?[0]["message"] as Any)
                    completionHandler(nil, data?[0]["message"])
                }
                else if  ((message  != nil && !(message?.isEmpty)! && (message?.keys.contains("message"))!)){
                    print(message?["message"] as Any)
                    completionHandler(nil, message?["message"])
                }
                else {
                    completionHandler(data, nil)
                }
            }
            else{
                print(response.result.error as Any)
                completionHandler(nil, response.result.error)
            }
        }
    }
    
    func SendHttpMethodArrayAsync(body : [String : Any]? = nil, auth : String? = nil, httpMethod : HTTPMethod, requestPath : String, completionHandler:@escaping (_ object: [Any]?, _ error : Any?) -> Void) {
        
        let url : String = apiUrl_ + requestPath
        
        var headers : [String : String] =  ["Content-Type" : "application/json", "X-Api-Key" : apiKey_ ]
        
        if (auth != nil){
            headers["Authorization"] = "Bearer " + auth!
        }
        
        Alamofire.request(url, method: httpMethod, parameters: body, encoding: JSONEncoding.default, headers: headers).responseJSON{
            response in
            if response.result.isSuccess{
                let data = response.result.value as? [[String: Any]]
                if (data != nil && !(data?.isEmpty)! && (data?[0].keys.contains("message"))!){
                    print(data?[0]["message"] as Any)
                    completionHandler(nil, data?[0]["message"])
                }
                else {
                    completionHandler(data, nil)
                }
            }
            else{
                print(response.result.error as Any)
                completionHandler(nil, response.result.error)
            }
        }
    }
}
