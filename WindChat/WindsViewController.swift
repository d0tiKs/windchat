//
//  WindViewController.swift
//  WindChat
//
//  Created by d0t_iKs on 12/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

class WindsViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, AVCapturePhotoCaptureDelegate{
    
    @IBOutlet weak var PreviewBack: UIView!
    
    @IBOutlet weak var DownArrayImageView: UIImageView!
    @IBOutlet weak var CameraImageView: UIImageView!

    
    var sessionBack: AVCaptureSession?
    var stillImageOutputBack: AVCapturePhotoOutput?
    var videoPreviewLayerBack: AVCaptureVideoPreviewLayer?
    var cameraBack = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
    
    var back = true
    var image : UIImage?
    
    var user : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetupGestures()
    }
    
    func SetupGestures(){
        let up = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes))
        up.direction = .up
        self.view.addGestureRecognizer(up)
        
        let touchCamera = UITapGestureRecognizer(target:self, action:#selector(TakeWind))
        touchCamera.numberOfTapsRequired = 1
        touchCamera.numberOfTouchesRequired = 1
        CameraImageView.isUserInteractionEnabled = true
        CameraImageView.addGestureRecognizer(touchCamera)
        
        let DownGesture = UITapGestureRecognizer(target:self, action:#selector(DownArrowTapped))
        DownGesture.numberOfTapsRequired = 1
        DownGesture.numberOfTouchesRequired = 1
        DownArrayImageView.isUserInteractionEnabled = true
        DownArrayImageView.addGestureRecognizer(DownGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initCamera()
        
    }
    
    func initCamera()
    {
        var input: AVCaptureDeviceInput!
        var error: NSError?
        do {
            sessionBack = AVCaptureSession()
            sessionBack?.sessionPreset = AVCaptureSessionPreset1920x1080

            input = try AVCaptureDeviceInput(device: cameraBack)
            
            if error == nil && sessionBack!.canAddInput(input) {
                sessionBack!.addInput(input)
                stillImageOutputBack = AVCapturePhotoOutput()
            }
            
            if sessionBack!.canAddOutput(stillImageOutputBack) {
                sessionBack!.addOutput(stillImageOutputBack)
                
                videoPreviewLayerBack = AVCaptureVideoPreviewLayer(session: sessionBack)
                videoPreviewLayerBack!.videoGravity = AVLayerVideoGravityResizeAspect
                videoPreviewLayerBack!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                PreviewBack.layer.addSublayer(videoPreviewLayerBack!)
                sessionBack!.startRunning()
            }
            
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        
    }
    
    func TakeWind(){
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                             kCVPixelBufferWidthKey as String: 160,
                             kCVPixelBufferHeightKey as String: 160,
                             ]
        settings.previewPhotoFormat = previewFormat
        self.stillImageOutputBack?.capturePhoto(with: settings, delegate: self)
    }
    
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let error = error {
            print(error.localizedDescription)
        }
        
        // I search for about 5 hours another way to do it in vain
        let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(photoSampleBuffer)
        let dataProvider = CGDataProvider(data: imageData as! CFData)
        let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
        image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.right)
        self.performSegue(withIdentifier: "SendSegue", sender: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        videoPreviewLayerBack?.frame = PreviewBack.bounds
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FriendsSegue" {
            let controller = segue.destination as! FriendsViewController
            controller.user = self.user
        }
        if segue.identifier == "SendSegue" {
            let controller = segue.destination as! SendWindController
            controller.user = self.user
            controller.image = image
        }
    }
    
    func handleSwipes(sender: UISwipeGestureRecognizer) {
        if (sender.direction == .up) {
            performSegue(withIdentifier: "FriendsSegue", sender: self)
        }
    }
    
    func DownArrowTapped(sender: UITapGestureRecognizer){
        performSegue(withIdentifier: "FriendsSegue", sender: self)
    }
}
