//
//  Date.swift
//  WindChat
//
//  Created by d0t_iKs on 09/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation

extension Date {

    public static func ISOStringFromDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT") as TimeZone!
        dateFormatter.dateFormat = "yyyy-MM-dd'T'"
        
        return dateFormatter.string(from: date as Date).appending("00:00:00")
    }
    
    public static func dateFromISOString(string: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return dateFormatter.date(from: string)!
    }
    
}
