//
//  User.swift
//  WindChat
//
//  Created by d0t_iKs on 09/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation
import ObjectMapper

class User : Mappable{

    var id : Int?
    var username : String?
    var firstName : String?
    var lastName : String?
    var birthday : String?
    var eMail : String?
    var password : String?

    private var picture : NSData?
    private var smallPicture : NSData?
    
    var imageStr64 : String?
    var pictureUrl : String?
    var smallPictureUrl : String?
    var token: String? = nil
    
    var winds : [Wind]?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    init(username : String? = nil, firstname : String? = nil, lastname : String? = nil, birthday : Date? = nil, email : String? = nil, password : String? = nil, picture : NSData? = nil){
        self.username = username
        self.firstName = firstname
        self.lastName = lastname
        if (birthday != nil){
            self.birthday = Date.ISOStringFromDate(date: birthday!)
        }
        self.eMail = email
        self.password = password
        self.picture = picture
        
        if (picture != nil){
            self.imageStr64 = picture!.base64EncodedString()
        }
    }
    
    convenience init(token : String, username : String? = nil, firstname : String? = nil, lastname : String? = nil, birthday : Date? = nil, email : String? = nil, password : String? = nil, pictureUrl : String? = nil){
        
        self.init(username: username, firstname: firstname, lastname: lastname, birthday: birthday, email: email, password: password)
        self.token = token
        self.pictureUrl = pictureUrl
    }

    func isEmpty() -> Bool{
        return username == nil && id == nil && firstName == nil && lastName == nil && birthday == nil;
    }
    
    func PictureGetAsync(completionHandler:@escaping(NSData?) -> Void){
        if (picture != nil){
            completionHandler(picture)
        }
        else if(pictureUrl != nil){
            let gateway = NetworkGateway.sharedInstance
            gateway.LoadImageAsync(url: pictureUrl!){
                data in
                self.picture = data
                completionHandler(self.picture)
            }
        }
        else{
            completionHandler(nil)
        }
    }
    
    func SmallPictureGetAsync(completionHandler:@escaping(NSData?) -> Void){
        if (smallPicture != nil){
            completionHandler(picture)
        }
        else if(smallPictureUrl != nil){
            let gateway = NetworkGateway.sharedInstance
                gateway.LoadImageAsync(url: smallPictureUrl!){
                data in
                self.smallPicture = data
                completionHandler(self.smallPicture)
            }
        }
        else{
            completionHandler(nil)
        }
    }
    
    func mapping(map: Map) {
        id              <-  map["id"]
        username        <-  map["userName"]
        firstName       <-  map["firstName"]
        lastName        <-  map["lastName"]
        birthday        <-  map["birthday"]
        eMail           <-  map["email"]
        password        <-  map["password"]
        imageStr64      <-  map["imageStr64"]
        pictureUrl      <-  map["pictureUrl"]
        smallPictureUrl <-  map["pictureUrlSmall"]
        token           <-  map["token"]
        winds           <-  map["winds"]
    }
}
