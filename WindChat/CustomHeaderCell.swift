//
//  CustomHeaderCell.swift
//  WindChat
//
//  Created by d0t_iKs on 18/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation
import UIKit

class CustomHeaderCell : UITableViewCell {
   
    @IBOutlet var headerLabel: UILabel!
}
