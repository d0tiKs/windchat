//
//  WindOpenerViewController.swift
//  WindChat
//
//  Created by d0t_iKs on 18/12/2016.
//  Copyright © 2016 pisu_a. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class WindOpenerViewController: UIViewController{
    
    var main : WindViewerViewController?
    var user : User?
    var wind : Wind?
    var timer : Timer?
    var count  = 0;
    
    @IBOutlet weak var ProgressBar: UIProgressView!
    @IBOutlet var WindImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        StartWaiting(title: "Loading Wind", message: "Please Wait")
        
        ProgressBar.setProgress(0, animated: false)
        
        wind?.ImageGetAsync{
            data in
            self.WindImageView.image = UIImage(data : data! as Data)
            self.StopWaiting()
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
        }
        OpenWindAsync()
    }
    
    func OpenWindAsync(){
        let gateway = NetworkGateway.sharedInstance
        gateway.Winds?.WindOpenAsync(user: user!, wind: wind!){
            result, error in
            if (result == false || result == nil || error != nil){
                print(error as Any)
                self.ShowAlert(title: "Open Wind", message: "Fail to contact server", button: "Dismiss")
            }
            self.main?.PopulateDataSource()
        }
    }
    
    func StartWaiting(title : String, message : String){
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity.label.text = title;
        spinnerActivity.detailsLabel.text = message;
        spinnerActivity.isUserInteractionEnabled = false;
    }
    
    func StopWaiting(){
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func timerAction() {
        count += 1
        let fcount : Float = Float(count)
        let fduration: Float = Float(wind!.duration!)
        ProgressBar.setProgress(fcount / fduration, animated: true)
        if (count == wind!.duration!){
            self.dismiss(animated: true, completion: nil)
        }
    }
}
